package sys;


/**
 * class MenuItem
 *
 * @author: Diuxx
 */
public enum MenuItem {
    NONE,
    EXITGAME,
    SAVEGAME,
    BACK;
}
